# Branching and Merging
Branching is the concept of going down a different path in the codebase from a specific commit. Usually branching off master in the latest state to make a particular set of changes and not modifying the state of the original code you branched from. Merging is the concept of applying changes made in one branch to another. 

![branching.png](https://leanpub.com/site_images/git-flow/git-workflow-release-cycle-2feature.png)

***Practical Example***  

* You need to make some changes to a project that may take some time. 
* You make a branch to do those changes and an emergency change comes in that has to be pushed live.
* You can switch back to master or whatever branch you are supposed to create your branches from when developing.
* Now you create another branch for the emergency changes and submit a pull request for approval.
* Switch back to your feature branch and continue working on the long task.


## Merging and Branching Commands

***`git branch`***  
Shows you what branches exist in your repository and what branch you are on. Your current branch is marked with a `*`

```
$ git branch
  Feature_123
  Feature_ABC
  Feature_XYZ
* master
```

***`git checkout`***  
Checkout out a commit, branch, tag, or file into your local working copy

```
$ git checkout Feature_123
Switched to branch 'Feature_123'
```

***`git checkout -b BRANCH_NAME` or `git branch BRANCH_NAME`***  
`git checkout -b` creates a branch and checks out that branch immediately. `git branch` with a branch name provided creates the branch but does not check it out.

***`git branch -d BRANCH_NAME`***  
Deletes a local branch from your working copy of the repository.

```
$ git branch -d Feature_XYZ
Deleted branch Feature_XYZ (was a6adcc2).
```

***`git merge BRANCH_NAME`***  
Merges the named branch into the branch you are currently on. All the changes that exist in the branch you are merging from now exist in your branch.

```
$ git merge Feature_123
Updating a6adcc2..a71b4f2
Fast-forward
 contact.html | 9 +++++++++
 1 file changed, 9 insertions(+)
```

### Merge Conflicts
A merge conflict happens when attempting to merge a branch into your current working branch or attempting to merge your current working branch into another. In most cases, Git will figure out how to integrate new changes. There's a handful of situations where you might have to step in and tell Git what to do. This typically happens when 2 developers have changed the same line in a file and git cannot determine who's changes are most relevant. Git will mark the file as having a conflict - which you'll have to solve before you can continue your work.

#### Git status output during showing conflict 
![git_status_conflict.png](git_status_on_merge.png)


**Resolving Merge Conflicts**

Take look at the contents of the conflicted file. Git was nice enough to mark the problematic area in the file by enclosing it in "<<<<<<< HEAD" and ">>>>>>> [other/branch/name]".

The contents after the first marker represent the current working branch (usually your code). After the angle brackets, Git tells us where (from which branch) the conflicting changes came from. A line with "=======" separates the differences.

***Example of file merge conflict markers***  

![conflict-markers.png](merge_conflict_markers.png)

Conflicts can be resolved by opening the file in an editor and cleaning up the file to reflect the relevant changes by removing the unwanted code. Once the file is cleaned up you would then do a regular commit to resolve the conflict. There are also git merge tools to help with the process of resolving a conflict. It's best to learn how to resolve a conflict manually so you can understand what those tools are doing behind the scenes.

## Branching Practice
Lets go ahead and practice branching by making a simple branch and modifying the index.html file in this repo.

* Fork and clone this repository to your local machine
* Run `git checkout -b AddHeader` to create a branch called AddHeader and checkout that branch
* Run `git status` and notice that you are now on the AddHeader branch

```
On branch AddHeader
nothing to commit, working directory clean
```

* Open up index.html and add a header using the `<h1>` 
* Run `git status` to show that we are on our branch and that the index.html file has been modified.

```
On branch AddHeader
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   index.html

no changes added to commit (use "git add" and/or "git commit -a")
```

* Run `git add index.html` to stage this file for commit
* Run `git commit -m "adding a heading to the website"` 
* Run `git log` and now you can see that new commit has been added
* Checkout the master branch with `git checkout master`
* Now look at the contents of ***index.html***. You should notice your header you added is gone. This is because that change only lives on your branch.
* Checkout the AddHeader Branch, `git checkout AddHeader`, and you should see your header again.

### Merging Practice
Now that we have made our simple change and can see that our code can vary from branch to branch, lets get our changes from our feature branch merged into our master branch. Once merged we can delete our branch.

**NOTE** ***You always want to merge master (or whatever branch you want to put your work into) into your branch first. This will help avoid merge conflicts in the master branch. This way we can clean up issues in our feature branch and then merge the clean code into master.***

* Make sure you are on the AddHeader branch, `git checkout AddHeader`
* Run `git merge master`. This will take any changes that have been made to master since you created the branch and add them to your branch.
* Now checkout master, `git checkout master`.
* Merge the AddHeader branch into the master branch, `git merge AddHeader`.
* This may popup a text editor for you to provide a message for the merge commit. Go ahead and save that file. 
* Now open the index.html file while on the master branch and notice the header has been added.
* Now you can delete your feature branch with `git branch -d AddHeader`

## Additional Resources
- https://learngitbranching.js.org/  